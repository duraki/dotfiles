" auto start nerdtree
autocmd vimenter * NERDTree

" disable fu..ng swapfiles
set noswapfile

" dont open nerdtree if vim file.ext is runned
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" hide nerdtree with ctrl+n
map <C-n> :NERDTreeToggle<CR>

" close vim if only nerdtree is visible
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" map for the search, to use cursor the word and press F4
map <F4> :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

" map go to create a curly brackets
nnoremap <C-g> i{<CR><CR>}

" solarized theme
syntax enable
set background=dark
colorscheme monokai
"colorscheme gruvbox
set number
filetype indent on
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" tagbar plugin
nmap <F8> :TagbarToggle<CR>

set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'altercation/vim-colors-solarized'
Plugin 'majutsushi/tagbar'
Plugin 'L9'
Plugin 'FuzzyFinder'
Plugin 'morhetz/gruvbox:'

" ALL PLUGINS SHOULD BE BETWEEN VUNDLE#BEGIN


" ALL PLUGINS SHOULD BE ABOVE VUNDLE#END

call vundle#end()
filetype plugin indent on


"execute pathogen#infect()
"call pathogen#helptags()
